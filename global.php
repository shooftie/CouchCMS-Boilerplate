<?php require_once( "manage/cms.php" ); ?>

<cms:template title="Global Settings" executable="0">

    <cms:editable name="company"
        label="Company"
        type="group"
        order="0" />

        <cms:editable name="company_name_full"
            label="Formal Company Name"
            desc="e.g. 'My Company &amp; Co. Ltd'"
            type="text"
            order="0"
            required="1"
            group="company" />

        <cms:editable name="company_name_short"
            label="Casual Company Name"
            desc="e.g. 'My Company'"
            type="text"
            order="1"
            required="1"
            group="company" />

        <cms:editable name="company_name_slug"
            label="Slug Company Name"
            desc="e.g. 'my-company' [auto]"
            type="text"
            order="2"
            required="0"
            group="company"><cms:php>echo(strtolower(str_replace(" ", "-", "<cms:show company_name_short />")));</cms:php></cms:editable>

        <cms:editable name="company_type"
            label="Company Type"
            desc="e.g. &quot;Inter-Galactic Import &amp; Export&quot;"
            type="text"
            order="3"
            required="1"
            group="company" />

    <cms:editable name="global_seo"
        label="Global SEO"
        desc="These can be overridden on each page"
        type="group"
        order="1" />

        <cms:editable name="global_seo_title"
            label="Page Title [auto]"
            desc="Will be displayed in the browser tab"
            type="text"
            order="1"
            group="global_seo"><cms:show company_name_short /> :: <cms:show company_type /></cms:editable>

        <cms:editable name="global_seo_description"
            label="Meta Description"
            desc="Short description of content the page - 140 words"
            type="textarea"
            order="2"
            required="1"
            group="global_seo"
            height="50px" />

        <cms:editable name="global_seo_url"
            label="Meta URL [auto]"
            desc="Link to the homepage"
            type="text"
            order="3"
            required="1"
            group="global_seo"><cms:show k_site_link /></cms:editable>

        <cms:editable name="global_seo_keywords"
            label="Meta Keywords"
            desc="About the content of the page - 7-8 max comma separated"
            type="text"
            order="4"
            required="1"
            group="global_seo" />

        <cms:editable name="global_seo_og_type"
            label="Open Graph Type"
            desc="The type of the page. See http://ogp.me/#types"
            type="radio"
            opt_values="article | book | profile | website"
            opt_selected="website"
            order="5"
            required="1"
            group="global_seo" />

        <cms:editable name="global_seo_site_name"
            label="Meta Site Name"
            desc="[auto]"
            type="text"
            order="6"
            required="1"
            group="global_seo"><cms:show company_name_full /></cms:editable>

    <cms:editable name="global_social"
        label="Global Social Media"
        desc="These can be overridden on each page"
        type="group"
        order="2" />

        <cms:editable name="global_social_image"
            label="Social media image"
            desc="Filename of image in the site '/img' directory [auto]"
            type="text"
            order="0"
            required="1"
            group="global_social">social-img.png</cms:editable>

        <cms:editable name="global_twitter_card_type"
            label="Twitter Card Type"
            desc="The type of the card. See https://dev.twitter.com/docs/cards [auto]"
            type="radio"
            opt_values="summary_large_image"
            opt_selected="summary_large_image"
            order="1"
            required="1"
            group="global_social" />
</cms:template>

<?php COUCH::invoke(); ?>
