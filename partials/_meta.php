<!-- basic meta -->

<cms:if seo_keywords != "">
    <meta name="keywords" content="<cms:show seo_keywords />">
</cms:if>

<cms:if seo_description != "">
    <meta name="description" content="<cms:show seo_description />">
</cms:if>

<!-- Open graph meta -->

<meta property="og:title" content="<cms:show seo_title />">

<cms:if seo_description != "">
    <meta property="og:description" content="<cms:show seo_description />">
</cms:if>

<cms:if seo_url != "">
    <meta property="og:url" content="<cms:show seo_url />">
</cms:if>

<cms:if social_image != "">
    <meta property="og:image" content="<cms:show k_site_link /><cms:show social_image />">
</cms:if>

<cms:if seo_og_type != "">
    <meta property="og:type" content="<cms:show seo_og_type />">
</cms:if>

<cms:if global_seo_site_name != "">
    <meta property="og:site_name" content="<cms:show global_seo_site_name />">
</cms:if>

<!-- Twitter meta -->

<meta name="twitter:card" content="summary_large_image">
